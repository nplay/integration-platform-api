import axios from "axios"

class Get
{
    constructor(storeURL)
    {
        this.url = storeURL + '/beai_category/index/move'
    }

    async request(tree)
    {
        return await axios({
            method: 'post',
            url: this.url,
            //headers: {},
            data: tree
        });
    }
}

export default Get
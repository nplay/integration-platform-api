import axios from "axios"

class Get
{
    constructor(storeURL)
    {
        this.url = storeURL + '/beai_category/index/tree'
    }

    async request()
    {
        let response = await axios.get(this.url)
        return response.data.data
    }
}

export default Get
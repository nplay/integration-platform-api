const { ServiceBroker } = require("moleculer");

import {Config} from "core"
import V1Service from './V1/service'

let broker = new ServiceBroker({
    nodeID: process.env.HOSTNAME,
    transporter: Config.queue.driver.rabbitMQ.connectionString
});

let fnRun = async () => {

    V1Service(broker)

    // Start server
    await broker.start();
}

fnRun()
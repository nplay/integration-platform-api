import { CategoryTreeHelper } from "./CategoryTreeHelper";

class Parser
{
    async execute(tree)
    {
        let treeHelper = new CategoryTreeHelper(tree)
        let iterator = treeHelper.iterate()
        let cursor  = iterator.next();
        let lastCursor = null;

        let output = new Array()

        while(cursor.done == false)
        {
            let catObj = new Object({
                categoryId: cursor.value.id,
                parentId: cursor.value.parent_id,
                afterId: lastCursor == null ? null : lastCursor.value.id
            })

            output.push(catObj)

            lastCursor = new Object(cursor)
            cursor = iterator.next()
        }

        return output
    }
}

export default Parser
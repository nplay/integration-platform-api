
import CategoryGet from '../../../category/api/get'

class Retrive
{
    async execute(content, message, ok, fail){
        try {
            let body = content.params
            let host = body.data.host
            let identifier = body.identifier

            if(!host || !identifier)
                throw new Error(`Host or identifier not defined`)

			let categoryGet = new CategoryGet(host)
            let categorys = await categoryGet.request()

            this.emit2({}, 'v1.ipapi.getCategoryResult', {
                identifier: identifier,
                categorys: categorys
            })
            
            ok(message)
        } catch (error) {
            console.log(error)
            fail(message)
        }
    }
}

export {
    Retrive
}
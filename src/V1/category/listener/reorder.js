
import CategoryMove from '../../../category/api/move'
import Parser from '../helper/Parser'

class Reorder
{
    async execute(content, message, ok, fail){

        let body = null
        let menuID = null
        let menu = null
        let host = null
        let identifier = null

        try {
            body = content.params
            menuID = body.menuID
            menu = body.menu
            host = body.host
            identifier = body.identifier

            if(!identifier)
                throw new Error(`Identifier not defined`)
            else if(!menu)
                throw new Error(`Menu not defined`)
            else if(!host)
                throw new Error(`Host not defined`)
            else if(!menuID)
                throw new Error(`menuID not defined`)

            let move = new CategoryMove(host)
            menu = await new Parser().execute(menu)
            let response = await move.request(menu)
            
            if(!response.data.success)
                throw new Error(response.data.message)

            this.emit2({}, 'v1.ipapi.reorderCategoryCompleted', {
                identifier: identifier,
                menuID: menuID
            })
            
            ok(message)

        } catch (error) {
            console.log(error)

            this.emit2({}, 'v1.ipapi.reorderCategoryFail', {
                identifier: identifier,
                menuID: menuID
            })
            
            fail(message)
        }
    }
}

export {
    Reorder
}

import events2 from 'event2'
import { Retrive } from './listener/retrive'
import { Reorder } from './listener/reorder'

export default {
    name: "ipapi",
    version: 1,
    mixins: [events2],
    actions: {
    },
    events2: [
        {
            event:'v1.category.get',
            listeners: {
                retrive: {
                    handle: new Retrive().execute,
                    createDeadLetter: false
                }
            }
        },
        {
            event:'v1.category.reorder',
            listeners: {
                request: {
                    handle: new Reorder().execute,
                    createDeadLetter: false
                }
            }
        }
    ]
}
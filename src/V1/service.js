
import CategoryService from './category/service'

export default async (broker) => {
    broker.createService(CategoryService)
}